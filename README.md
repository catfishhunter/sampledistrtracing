# Пример распределенной трассировки на vert.x

----
запускаем Jaeger:

```docker run -d --rm --name jaeger -p 5775:5775/udp -p 6831:6831/udp -p 6832:6832/udp -p 5778:5778 -p 16686:16686 -p 14268:14268 -p 9411:9411 jaegertracing/all-in-one:1.10```

запуск 2 приложений:

```SubAppKt```
```AppKt```


----
открываем интерфейс jaeger:

```http://localhost:16686```


