package ms2

import io.opentracing.Tracer
import io.opentracing.util.GlobalTracer
import io.vertx.core.AbstractVerticle
import io.vertx.tracing.opentracing.OpenTracingUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicInteger


class RestMs : AbstractVerticle() {
    private val logger: Logger = LoggerFactory.getLogger(RestMs::class.java)

    val i:AtomicInteger= AtomicInteger()


    override fun start() {
        val tracer = SubApp.tracer
        vertx.createHttpServer().requestHandler({
                request ->
            val pSpan=OpenTracingUtil.getSpan()
            val span=tracer.buildSpan("calculate").asChildOf(pSpan).start()
            span.setTag("thread",Thread.currentThread().toString())

            OpenTracingUtil.setSpan(span)
            request.response().end(sameCalculate())
            span.finish()
            OpenTracingUtil.setSpan(pSpan)
        }).listen(8088)
    }

    private fun sameCalculate() ="calculated"

}


