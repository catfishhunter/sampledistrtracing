package ms2

import io.jaegertracing.Configuration
import io.jaegertracing.internal.JaegerTracer
import io.opentracing.Tracer
import io.opentracing.util.GlobalTracer
import io.vertx.core.Vertx
import io.vertx.core.VertxOptions
import io.vertx.core.tracing.TracingOptions
import io.vertx.tracing.opentracing.OpenTracingOptions
import org.slf4j.LoggerFactory
import io.vertx.tracing.opentracing.OpenTracingTracer
import ms1.App


public class SubApp {
    private val logger = LoggerFactory.getLogger(SubApp::class.java)

    companion object {
        //val tracer: Tracer= OpenTracingTracer.createDefaultTracer()
        //val tracer: Tracer=  GlobalTracer.get()

        val tracer: Tracer = newTrace()

        fun newTrace(): Tracer {
            //tracer  = OpenTracingTracer.createDefaultTracer()
            val samplerConfig = Configuration.SamplerConfiguration.fromEnv()//.withType("const").withParam(1)
            val reporterConfig = Configuration.ReporterConfiguration.fromEnv().withLogSpans(true)
            val config = Configuration("MS2 vertx").withSampler(samplerConfig).withReporter(reporterConfig)
            val tracer = config.tracer
            //val tracer = JaegerTracer.Builder("MS2j vertx").build()
            return tracer
        }
    }
}

public fun main(args: Array<String>) {
    //GlobalTracer.registerIfAbsent(SubApp.tracer)
    val openTracingOptions = OpenTracingOptions(SubApp.tracer)


    val opts = VertxOptions().setTracingOptions(openTracingOptions)

    println("opts.eventLoopPoolSize ${opts.eventLoopPoolSize}")
    println("opts.workerPoolSize ${opts.workerPoolSize}")
    println("opts.internalBlockingPoolSize ${opts.internalBlockingPoolSize}")
    val vertx = Vertx.vertx(opts)
    vertx.deployVerticle("ms2.RestMs")
}
