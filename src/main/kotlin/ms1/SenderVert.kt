package ms1

import io.opentracing.Tracer
import io.vertx.core.AbstractVerticle
import io.vertx.tracing.opentracing.OpenTracingUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicInteger


class SenderVert : AbstractVerticle() {
    private val logger: Logger = LoggerFactory.getLogger(SenderVert::class.java)

    val i:AtomicInteger= AtomicInteger()


    override fun start() {
        val tracer = App.tracer//GlobalTracer.get()//Configuration.fromEnv().withServiceName("sample").tracer

        vertx.periodicStream(5000).handler(
            {
                send( tracer)
                send( tracer)
                send( tracer)
                send( tracer)
                send( tracer)
            }
        )
    }

    private fun send(tracer: Tracer) {
        val span = tracer.buildSpan("maint operation")/*.ignoreActiveSpan()*/.start()
        //val scope=tracer.activateSpan(span)
        OpenTracingUtil.setSpan(span)
        val payl: String = "S message ${i.incrementAndGet()}"
        span.setTag("message", payl)
        span.setTag("thread",Thread.currentThread().toString())
        logger.info("send $payl $tracer ${Thread.currentThread()}")
        vertx.eventBus().publish("my", payl)
        vertx.eventBus().publish("my", payl)
        span.finish()
        //scope.close()
    }

}


