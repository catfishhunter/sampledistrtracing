package ms1

import io.opentracing.Tracer
import io.vertx.core.AbstractVerticle
import io.vertx.tracing.opentracing.OpenTracingUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicInteger


class RestVert : AbstractVerticle() {
    private val logger: Logger = LoggerFactory.getLogger(RestVert::class.java)

    val i:AtomicInteger= AtomicInteger()


    override fun start() {
        val tracer = App.tracer//GlobalTracer.get()//Configuration.fromEnv().withServiceName("sample").tracer

        vertx.periodicStream(1000).handler(
            {
                send( tracer)
            }
        )

    }

    private fun send(tracer: Tracer) {
        val span = tracer.buildSpan("mainOperation").ignoreActiveSpan().start()
        //val scope=tracer.activateSpan(span)
        OpenTracingUtil.setSpan(span)
        val payl: String = "message ${i.incrementAndGet()}"
        span.setTag("message", payl)
        logger.info("send $payl $tracer ${Thread.currentThread()}")
        vertx.eventBus().publish("my", payl)
        span.finish()
        //scope.close()
    }

}


