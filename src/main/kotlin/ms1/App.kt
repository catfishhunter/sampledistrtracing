package ms1

import io.jaegertracing.Configuration
import io.jaegertracing.internal.JaegerTracer
import io.opentracing.Tracer
import io.opentracing.util.GlobalTracer
import io.vertx.core.Vertx
import io.vertx.core.VertxOptions
import io.vertx.core.tracing.TracingOptions
import io.vertx.tracing.opentracing.OpenTracingOptions
import org.slf4j.LoggerFactory

public class App {
    private val logger = LoggerFactory.getLogger(App::class.java)
    companion object{
        //val tracer: Tracer= OpenTracingTracer.createDefaultTracer()
        //val tracer: Tracer=  GlobalTracer.get()
        val tracer: Tracer = newTrace()
            fun newTrace(): Tracer {
            //tracer  = OpenTracingTracer.createDefaultTracer()
            val samplerConfig = Configuration.SamplerConfiguration.fromEnv()//.withType("const").withParam(1)
            val reporterConfig = Configuration.ReporterConfiguration.fromEnv().withLogSpans(true)
            val config = Configuration("MS1 vertx").withSampler(samplerConfig).withReporter(reporterConfig)
            val tracer=config.tracer
            //val tracer = JaegerTracer.Builder("MS1j vertx").build()
        return tracer
        }
        //Configuration.fromEnv().withServiceName("sample").tracer
    }
}

     public fun main(args: Array<String>) {
     //val tracer = Configuration.fromEnv().withServiceName("sample").tracer
     //GlobalTracer.register(tracer)
     val opts=VertxOptions().setTracingOptions(OpenTracingOptions(App.tracer))
         println("opts.eventLoopPoolSize ${opts.eventLoopPoolSize}")
         println("opts.workerPoolSize ${opts.workerPoolSize}")
         println("opts.internalBlockingPoolSize ${opts.internalBlockingPoolSize}")
     val vertx = Vertx.vertx(opts)
    // vertx.deployVerticle("ms1.RestVert")
     vertx.deployVerticle("ms1.Pebble")
     vertx.deployVerticle("ms1.SenderVert")
 }
