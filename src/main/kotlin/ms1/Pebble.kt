package ms1

import io.vertx.core.AbstractVerticle
import io.vertx.core.Handler
import io.vertx.core.eventbus.Message
import io.vertx.ext.web.client.WebClient
import io.vertx.tracing.opentracing.OpenTracingUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit

class Pebble : AbstractVerticle() {
    private val logger: Logger = LoggerFactory.getLogger(Pebble::class.java)


    public override fun start(){
        val webClient = WebClient.create(vertx);
        val handler: Handler<Message<String>> = Handler {
                m->
            val tracer = App.tracer
            val pspan=OpenTracingUtil.getSpan()
            val span=tracer.buildSpan("pebble operation").asChildOf(pspan).start()
            //var scope = tracer.activateSpan(span)
            OpenTracingUtil.setSpan(span)
            TimeUnit.MILLISECONDS.sleep(0)
            logger.info("pebble span${m.body()}  $tracer ${Thread.currentThread()}")
            span.setTag("thread",Thread.currentThread().toString())
            span.log(m.body())

            webClient.get(8088,"localhost","/").send( {ar->
            if (ar.succeeded()) logger.info("returned ${ar.result().body()}")
            else logger.error(ar.cause().message)
        })
            span.finish()
            //scope.close()
            OpenTracingUtil.setSpan(pspan)
        }

        vertx.eventBus().consumer("my",handler)



    }


}